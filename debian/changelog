liblwp-mediatypes-perl (6.04-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.4.1, no changes needed.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 17:22:44 +0000

liblwp-mediatypes-perl (6.04-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team. Closes: #924989
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ intrigeri ]
  * Add debian/upstream/metadata
  * New upstream release
  * Bump debhelper compatibility version to 12.
  * Declare compatibility with Debian Policy 4.4.0.
  * Add build-dependency on libtest-fatal-perl.
  * Enable autopkgtests.
  * Drop versioned Breaks+Replaces that's satisfied in oldoldstable.
  * Add myself to Uploaders (we had none anymore).

 -- intrigeri <intrigeri@debian.org>  Thu, 18 Jul 2019 20:51:49 +0000

liblwp-mediatypes-perl (6.02-1) unstable; urgency=low

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Alessandro Ghedini ]
  * New upstream release
  * Bump Standards-Version to 3.9.2 (no changes needed)
  * Bump debhelper compat level to 8

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 17 Feb 2012 14:12:17 +0100

liblwp-mediatypes-perl (6.01-1) unstable; urgency=low

  * Initial Release (Closes: #617646).

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Thu, 10 Mar 2011 09:32:41 +0000
